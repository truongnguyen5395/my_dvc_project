import argparse
from sklearn.model_selection import train_test_split
from typing import Text
import yaml
import pandas as pd

def data_split(config_path: Text) -> None:
    """
    Splitting train test data

    :param config_path: the configuration path
    :return:
    """

    config = yaml.safe_load(open(config_path))
    feature_dataset = config['features_PIPELINE']['features_path']

    test_size = config['dataSplit_PIPELINE']['test_size']
    train_path = config['dataSplit_PIPELINE']['train_path']
    test_path = config['dataSplit_PIPELINE']['test_path']

    random_state = config['base']['random_state']


    dataset = pd.read_csv(feature_dataset)
    train_set, test_set = train_test_split(dataset, test_size=test_size, random_state=random_state)

    train_set.to_csv(train_path, index=False)
    test_set.to_csv(test_path, index=False)


if __name__ == "__main__":

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--config", dest="config", required=True)
    args = args_parser.parse_args()

    data_split(args.config)