import argparse
import joblib
import json
import os
import pandas as pd
from typing import Text, Dict, List
import yaml

from src.data.dataset import get_target_names
from src.evaluate.evaluate import evaluate
from src.report.visualize import plot_confusion_matrix

def evaluation(config_path: Text) -> None:
    """
    Evaluation model

    :param config_path: Configuration Path
    :return:
    """

    config = yaml.safe_load(open(config_path))
    target_column = config['features_PIPELINE']['target_column']
    model_name = config['base']['model']['model_name']
    model_path = config['base']['model']['model_path']
    report_path = config['base']['reports']['reports_path']
    test_path = config['dataSplit_PIPELINE']['test_path']

    model = joblib.load(os.path.join(model_path, model_name))

    test_df = pd.read_csv(test_path)
    report = evaluate(df=test_df,
                      target_column=target_column,
                      clf=model)
    # print(report)
    classes = get_target_names()

    # save f1 metric file
    metrics_path = os.path.join(report_path, config['evaluation_PIPELNE']['metrics_file'])

    json.dump(
        obj={"f1_score": report['f1']},
        fp=open(metrics_path, 'w')
    )
    print(f"F1 metrics file saved to: {metrics_path}")

    # save confusion_matrix.png
    confusion_matrix_png_path = os.path.join(report_path, config['evaluation_PIPELNE']['confusion_matrix_png'])
    plt = plot_confusion_matrix(cm=report['cm'],
                                target_names=get_target_names(),
                                save_path=confusion_matrix_png_path,
                                normalize=False)
    # print("TRUONG OI: ", confusion_matrix_png_path)
    # plt.savefig(confusion_matrix_png_path)
    print(f'Confusion matrix saved to: {confusion_matrix_png_path}')

    # save confusion_matrix.json
    class_path = os.path.join(report_path, config['evaluation_PIPELNE']['classes_path'])
    mapping = {
        0: classes[0],
        1: classes[1],
        2: classes[2]
    }
    df = (pd.DataFrame({'actual': report['actual'],
                        'predicted': report['predicted']})
          .assign(actual= lambda x: x.actual.map(mapping))
          .assign(predicted= lambda x: x.predicted.map(mapping))
          )
    df.to_csv(class_path, index=False)
    print(f'Classes actual/predicted saved to: {class_path}')

if __name__ == "__main__":

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--config", dest="config", required=True)
    args = args_parser.parse_args()

    evaluation(args.config)