import argparse

from sklearn.linear_model import LogisticRegression
import pandas as pd
from typing import Text
import yaml
import joblib
import os

from src.train.train import train

def train_model(config_path: Text) -> None:
    """
    Training pipeline using Logistic Regression

    :param config_path: Configuration path
    :return:
    """

    config = yaml.safe_load(open(config_path))
    # This estimator name will help to switch between logreg or SVM
    estimator_name = config['train_PIPELINE']['estimator_name']
    param_grid = config['train_PIPELINE']['estimator'][estimator_name]['param_grid']
    cv = config["train_PIPELINE"]["cv"]
    target_column = config['features_PIPELINE']['target_column']
    train_path = config['dataSplit_PIPELINE']['train_path']

    train_df = pd.read_csv(train_path)
    model = train(
        df=train_df,
        target_column=target_column,
        estimator_name=estimator_name,
        param_grid=param_grid,
        cv=cv
    )
    print(model.best_score_)

    model_name = config['base']['model']['model_name']
    model_path = config['base']['model']['model_path']

    joblib.dump(
        model,
        os.path.join(model_path, model_name)
    )


if __name__ == "__main__":

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--config", dest="config", required=True)
    args = args_parser.parse_args()

    train_model(args.config)
