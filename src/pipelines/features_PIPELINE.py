import argparse
import pandas as pd
from typing import Text
import yaml

from src.features.features import extract_features

def featurize(config_path: Text) -> None:
    """
    Create new feature
    :param config_path: Path to config
    :return:
    """

    # Adding the config file
    config = yaml.safe_load(open(config_path))
    dataset_filepath = config['dataLoad_PIPELINE']['dataset_csv']
    feature_filepath = config['features_PIPELINE']['features_path']

    # Load the previous dataset
    dataset = pd.read_csv(dataset_filepath)
    # Call module for doing feature extract
    feature_dataset = extract_features(dataset)
    # Export the featurized dataset
    feature_dataset.to_csv(feature_filepath, index=False)

if __name__ == "__main__":

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    featurize(args.config)