import argparse
from typing import Text
import yaml

from src.data.dataset import get_dataset

def data_load(config_path: Text) -> None:

    config = yaml.safe_load(open(config_path))
    dataset = get_dataset()
    dataset.to_csv(config['dataLoad_PIPELINE']['dataset_csv'], index=False)

if __name__ == '__main__':
    """
    The argparse module makes it easy to write user-friendly command-line interfaces.
    
    name or flags - Either a name or a list of option strings, e.g. foo or -f, --foo.
    dest - The name of the attribute to be added to the object returned by parse_args().
    required - Whether or not the command-line option may be omitted (optionals only).
    """

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_load(config_path=args.config)